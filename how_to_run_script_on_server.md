# How to run a python code on server using PUTTY that uploads video on youtube?

*	Download an install PUTTY on your system, for 64-bit windows you can click on [https://the.earth.li/~sgtatham/putty/latest/w64/putty-64bit-0.70-installer.msi](https://the.earth.li/~sgtatham/putty/latest/w64/putty-64bit-0.70-installer.msi) to get it.

*	Login to your server

*	pip install httplib2 (Python2)

*	pip install --upgrade google-api-python-client

*	Create any directory and navigate inside it using PUTTY

*	Upload Python code(upload_video.py), Video(recorded/downloaded), json file(upload_video.py-oauth2.json, which can be downloaded from your "Google APIs project" => "OAuth 2.0 client IDs" => Click on 'DOWNLOAD JSON') and rename it to upload_video.py-oauth2.json

*	Run the code as follows (Don't forget to replace the values with your own).

```
python upload_video.py --file="rishikesh67.mp4" --title="HyGo - A site for Python/Golang" --description="rishikesh67.pythonanywhere.com is a websit for Golang/Python" --keywords="Rishikesh67"  --category="22" --privacyStatus="public"
```

