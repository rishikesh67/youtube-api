'''
    This script will attempt to open your webbrowser,
    perform OAuth 2 authentication and print your access token.
    It depends on two libraries: oauth2client and gflags.
    To install dependencies from PyPI:
    $ pip install python-gflags oauth2client
    Then run this script:
    $ python get_oauth2_token.py
    
    This is a combination of snippets from:
    https://developers.google.com/api-client-library/python/guide/aaa_oauth
'''

# https://gist.github.com/burnash/6771295
# https://github.com/burnash/gspread/wiki/How-to-get-OAuth-access-token-in-console%3F

from oauth2client.client import OAuth2WebServerFlow
from oauth2client.tools import run_flow
from oauth2client.file import Storage

# CLIENT_ID = '<Client ID from Google API Console>'
# CLIENT_SECRET = '<Client secret from Google API Console>'

CLIENT_ID = '959388467591-cnm0q50n9fl0e7sl0esjiiceb3ltlhpt.apps.googleusercontent.com'
CLIENT_SECRET = 'FR04lc6PphxNS2sE-SVdJw5p'

flow = OAuth2WebServerFlow(client_id=CLIENT_ID,
                           client_secret=CLIENT_SECRET,
                           scope='https://spreadsheets.google.com/feeds https://docs.google.com/feeds',
                           redirect_uri='http://example.com/auth_return')

storage = Storage('creds.data')

credentials = run_flow(flow, storage) # run(flow, storage) <= doesn't work

print "access_token: %s" % credentials.access_token