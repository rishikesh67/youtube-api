import mechanize
import cookielib

#Dealing with Unicode encoding....
import sys
reload(sys)  
sys.setdefaultencoding('utf8')

#Emulate browser....
br = mechanize.Browser()

# Set Cookie Jar so we can stay logged in...

cj = cookielib.LWPCookieJar()
br.set_cookiejar(cj)

#Other variables setting...

br.set_handle_equiv( True ) 
br.set_handle_gzip( True ) 
br.set_handle_redirect( True ) 
br.addheaders = [('User-agent', 'Firefox')]
br.set_handle_referer( True ) 
br.set_handle_robots( False )

#Go for google login
br.open('https://accounts.google.com/Login')
#Choose form
br.form = list(br.forms())[0] 

#Set value for "Email" 
br["Email"] = "rishikesh0014051992@gmail.com"
br.submit()
br.response().read()
#Set "Passwd"
br.form = list(br.forms())[0]
br["Passwd"] = "Rishi@67"
br.submit()

#Check that it worked...
br.open("https://mail.google.com/mail")
print br.response().read()

#Let`s take a look at the Cookies that we have just got...

print br.response().read()
for c in cj:
    print c