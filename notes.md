#	Yt

[https://developers.google.com/youtube/v3/guides/uploading_a_video](https://developers.google.com/youtube/v3/guides/uploading_a_video)

[https://developers.google.com/youtube/v3/getting-started](https://developers.google.com/youtube/v3/getting-started)

[https://developers.google.com/youtube/v3/quickstart/python](https://developers.google.com/youtube/v3/quickstart/python);

### Nice link

[https://developers.google.com/youtube/v3/code_samples/python#upload_a_video](https://developers.google.com/youtube/v3/code_samples/python#upload_a_video)


## API KEY

AIzaSyDWgHU2UuwujNNhMByQi6bFTn7fAkZXa9g

## OAuth

	[https://developers.google.com/fit/android/get-api-key#request_an_oauth_20_client_id_in_the_console_name](https://developers.google.com/fit/android/get-api-key#request_an_oauth_20_client_id_in_the_console_name)


	*	CLIENT ID (9...m)
959388467591-cnm0q50n9fl0e7sl0esjiiceb3ltlhpt.apps.googleusercontent.com

	*	CLIENT SECRET (F...p)
FR04lc6PphxNS2sE-SVdJw5p

## client_secrets.json

```
{
  "web": {
    "client_id": "[[INSERT CLIENT ID HERE]]",
    "client_secret": "[[INSERT CLIENT SECRET HERE]]",
    "redirect_uris": [],
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://accounts.google.com/o/oauth2/token"
  }
}
```



## What to do?

```
pip install --upgrade google-api-python-client
```

## How to run to upload video (Python 2 code)

```
# python upload_video.py --file="python-list2018-03-09-16-17-05.mp4" --title="Python list" --description="A list is just like an array" --keywords="Python3,Python,List"  --category="22" --privacyStatus="private"
# https://youtu.be/9R-_OucCyIo

# http://localhost:8080/?code=4/AAAU4Mx3wxdpvMJDzX_38yKtQr_YbTT_WWSgJUR4WIE0v_T_xxP1HjIRZbXld_nXIST2BzyPxalU8TBjMuaaspk#



# python upload_video.py --file="conda-2018-03-09-18-16-53.mp4" --title="Conda - basic commands" --description="conda is a powerful tool for environment and package management" --keywords="Conda,Anaconda5.1,Anaconda,Basic commands"  --category="22" --privacyStatus="public"
# pnWRwWZ59FQ
# https://youtu.be/pnWRwWZ59FQ
```
